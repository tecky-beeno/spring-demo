import express from 'express'
import { print } from 'listening-on'
import httpProxy from 'http-proxy'

let app = express()

app.use(express.static('public'))
// app.use(express.json())
// app.use(express.urlencoded({ extended: false }))

let proxy = httpProxy.createProxyServer({ target: 'http://localhost:8080' })

app.use((req, res, next) => {
  proxy.web(req, res)
})

let port = 8100
app.listen(port, () => {
  print(port)
})
