# restful spring server with cors demo

Both of below settings are required to make spring API works with CORS:

1. set `http.cors().and().csrf().disable();` in the `securityFilterChain`
2. add `@CrossOrigin()` in the controller class or controller method

If for some reason you cannot get around the cors issue
but already _disabled csrf_,
you can use the http-proxy as workaround.

## Demo Setup

- spring server listening on http://localhost:8080
- live server (in vscode) listening on http://127.0.0.1:5500
- http-proxy server listening on http://localhost:8100 (optional)

The 'web' directory is a standalone frontend

The 'web/public/index.html' can be served from the 'live server' vscode extension.

It can be served from the express server inside 'web' as well.
