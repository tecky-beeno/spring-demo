package com.example.springdemo.controller;

import com.example.springdemo.dto.LoginDTO;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin()
public class UserController {
    @PostMapping("/login")
    String login(@RequestBody LoginDTO body){
        System.out.println("login: " + body);
        return "echo: " +  body;
    }
    @GetMapping("/profile")
    String getProfile(){
        return "need login?";
    }
}


