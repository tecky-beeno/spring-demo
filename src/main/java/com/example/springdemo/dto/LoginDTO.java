package com.example.springdemo.dto;

public class LoginDTO {
    public String username;
    public String password;

    @Override
    public String toString() {
        return "LoginDTO{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
